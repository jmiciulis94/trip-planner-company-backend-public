package eu.sts.tripplannercompany.backend.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "Employee")
@Table(name = "employee")
public class Employee {

    public Employee() {}

    public Employee(Long id, Long cardId, String firstName, String lastName, String phone, Department department) {
        this.id = id;
        this.cardId = cardId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.department = department;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "card_id", nullable = false)
    private Long cardId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "phone", length = 15)
    private String phone;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "department_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Department department;
}
