package eu.sts.tripplannercompany.backend.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity(name = "Company")
@Table(name = "company")
public class Company {

    public Company() {}

    public Company(Long id, String address, String city, Long companyCode, String email, String name, String phone, Long postalCode, Long vatCode, List<Department> departments) {
        this.id = id;
        this.address = address;
        this.city = city;
        this.companyCode = companyCode;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.postalCode = postalCode;
        this.vatCode = vatCode;
        this.departments = departments;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "company_code", nullable = false)
    private Long companyCode;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "phone", length = 15, nullable = false)
    private String phone;

    @Column(name = "postal_code")
    private Long postalCode;

    @Column(name = "vat_code", nullable = false)
    private Long vatCode;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "company")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    private List<Department> departments = new ArrayList<>();
}
