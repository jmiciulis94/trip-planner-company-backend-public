package eu.sts.tripplannercompany.backend.entities;

import eu.sts.tripplannercompany.backend.constants.Role;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import static eu.sts.tripplannercompany.backend.constants.Role.ADMIN;
import static eu.sts.tripplannercompany.backend.constants.Role.SUPER_ADMIN;
import static eu.sts.tripplannercompany.backend.constants.Role.USER;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "Account")
@Table(name = "account")
public class Account {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    private Role role = ADMIN;

    @Column(name = "phone", length = 15)
    private String phone;

    @Column(name = "displayName")
    private String displayName;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Account(Long id, String firstName, String lastName, String email, String password, String role, String phone, String avatar) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = formatToRole(role);
        this.phone = phone;
        this.avatar = avatar;
    }

    private Role formatToRole(String role) {
        switch (role) {
            case "SuperAdmin":
                return SUPER_ADMIN;
            case "User":
                return USER;
            default:
                return ADMIN;
        }
    }
}
