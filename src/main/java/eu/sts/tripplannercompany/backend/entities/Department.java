package eu.sts.tripplannercompany.backend.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity(name = "Department")
@Table(name = "department")
public class Department {

    public Department() {}

    public Department(Long id, String name, Company company, List<Employee> employees) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.employees = employees;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "company_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Company company;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "department")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    private List<Employee> employees = new ArrayList<>();
}
