package eu.sts.tripplannercompany.backend.dto;

import java.util.List;
import lombok.Data;

@Data
public class CompanyDto {

    private final Long id;
    private final String address;
    private final String city;
    private final Long companyCode;
    private final String email;
    private final String name;
    private final String phone;
    private final Long postalCode;
    private final Long vatCode;
    private final List<DepartmentDto> departments;
}
