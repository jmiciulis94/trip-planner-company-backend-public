package eu.sts.tripplannercompany.backend.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class AccountDto {

    private final Long id;
    private final String firstName;
    private final String lastName;
    @Size(max = 256)
    @Pattern(regexp = "^$|^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private final String email;
    private final String password;
    private final String role;
    @Size(max = 15)
    @Pattern(regexp = "^$|^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?" +
            "([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)$")
    private final String phone;
    private final String avatar;
}
