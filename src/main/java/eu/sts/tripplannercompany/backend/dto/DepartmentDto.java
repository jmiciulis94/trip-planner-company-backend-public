package eu.sts.tripplannercompany.backend.dto;

import java.util.List;
import lombok.Data;

@Data
public class DepartmentDto {

    private final Long id;
    private final String name;
    private final Long companyId;
    private final String companyName;
    private final List<EmployeeDto> employees;
}
