package eu.sts.tripplannercompany.backend.dto;

import lombok.Data;

@Data
public class EmployeeDto {

    private final Long id;
    private final Long cardId;
    private final String firstName;
    private final String lastName;
    private final String phone;
    private final Long departmentId;
    private final String departmentName;
}
