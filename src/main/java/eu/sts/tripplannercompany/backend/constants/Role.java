package eu.sts.tripplannercompany.backend.constants;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Role {

    @JsonProperty("Admin")
    ADMIN,
    @JsonProperty("SuperAdmin")
    SUPER_ADMIN,
    @JsonProperty("User")
    USER
}


