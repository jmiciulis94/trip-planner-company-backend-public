package eu.sts.tripplannercompany.backend.repositories;

import eu.sts.tripplannercompany.backend.entities.Account;
import java.util.List;
import org.springframework.data.repository.Repository;

public interface AccountRepository extends Repository<Account, Long> {

    Account save(Account account);

    Account findByIdAndDeletedFalse(Long id);

    List<Account> findAllByDeletedFalse();

}
