package eu.sts.tripplannercompany.backend.repositories;

import eu.sts.tripplannercompany.backend.entities.Company;
import java.util.List;
import org.springframework.data.repository.Repository;

public interface CompanyRepository extends Repository<Company, Long> {

    Company save(Company company);

    Company findByIdAndDeletedFalse(Long id);

    List<Company> findAllByDeletedFalse();
}
