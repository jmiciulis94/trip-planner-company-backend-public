package eu.sts.tripplannercompany.backend.repositories;

import eu.sts.tripplannercompany.backend.entities.Employee;
import java.util.List;
import org.springframework.data.repository.Repository;

public interface EmployeeRepository extends Repository<Employee, Long> {

    Employee save(Employee employee);

    Employee findByIdAndDeletedFalse(Long id);

    List<Employee> findAllByDeletedFalse();

    List<Employee> findAllByDepartmentIdAndDeletedFalse(Long departmentId);
}
