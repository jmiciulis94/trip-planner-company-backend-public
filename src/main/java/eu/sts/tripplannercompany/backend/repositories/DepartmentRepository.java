package eu.sts.tripplannercompany.backend.repositories;

import eu.sts.tripplannercompany.backend.entities.Department;
import java.util.List;
import org.springframework.data.repository.Repository;

public interface DepartmentRepository extends Repository<Department, Long> {

    Department save(Department department);

    Department findByIdAndDeletedFalse(Long id);

    List<Department> findAllByDeletedFalse();

    List<Department> findAllByCompanyIdAndDeletedFalse(Long companyId);
}
