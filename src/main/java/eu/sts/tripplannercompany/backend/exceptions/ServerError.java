package eu.sts.tripplannercompany.backend.exceptions;

import java.util.List;

public class ServerError {

    private String header;
    private List<String> items;

    public ServerError() {
    }

    public ServerError(String header, List<String> items) {
        this.header = header;
        this.items = items;
    }
}
