package eu.sts.tripplannercompany.backend.services;

import eu.sts.tripplannercompany.backend.dto.EmployeeDto;
import eu.sts.tripplannercompany.backend.entities.Department;
import eu.sts.tripplannercompany.backend.entities.Employee;
import eu.sts.tripplannercompany.backend.repositories.DepartmentRepository;
import eu.sts.tripplannercompany.backend.repositories.EmployeeRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;

    public EmployeeService(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeDto> getEmployees() {
        return employeeRepository.findAllByDeletedFalse().stream().map(this::convertEmployeeToDto).collect(Collectors.toList());
    }

    public EmployeeDto getEmployee(Long id) {
        return convertEmployeeToDto(employeeRepository.findByIdAndDeletedFalse(id));
    }

    public EmployeeDto saveEmployee(EmployeeDto employeeDto) {
        return convertEmployeeToDto(employeeRepository.save(convertEmployeeDtoToEntity(employeeDto)));
    }

    public void deleteEmployee(Long id) {
        Employee employee = employeeRepository.findByIdAndDeletedFalse(id);
        employee.setDeleted(true);
        employeeRepository.save(employee);
    }

    public EmployeeDto convertEmployeeToDto(Employee employee) {
        return new EmployeeDto(employee.getId(), employee.getCardId(), employee.getFirstName(), employee.getLastName(), employee.getPhone(), employee.getDepartment().getId(), employee.getDepartment().getName());
    }

    public Employee convertEmployeeDtoToEntity(EmployeeDto employeeDto) {
        Department department = departmentRepository.findByIdAndDeletedFalse(employeeDto.getDepartmentId());
        return new Employee(employeeDto.getId(), employeeDto.getCardId(), employeeDto.getFirstName(), employeeDto.getLastName(), employeeDto.getPhone(), department);
    }
}
