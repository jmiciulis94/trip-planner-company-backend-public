package eu.sts.tripplannercompany.backend.services;

import eu.sts.tripplannercompany.backend.dto.CompanyDto;
import eu.sts.tripplannercompany.backend.dto.DepartmentDto;
import eu.sts.tripplannercompany.backend.entities.Company;
import eu.sts.tripplannercompany.backend.entities.Department;
import eu.sts.tripplannercompany.backend.entities.Employee;
import eu.sts.tripplannercompany.backend.repositories.CompanyRepository;
import eu.sts.tripplannercompany.backend.repositories.DepartmentRepository;
import eu.sts.tripplannercompany.backend.repositories.EmployeeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final DepartmentService departmentService;
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, DepartmentService departmentService, DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.departmentService = departmentService;
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyDto> getCompanies() {
        return companyRepository.findAllByDeletedFalse().stream().map(this::convertCompanyToDto).collect(Collectors.toList());
    }

    public CompanyDto getCompany(Long id) {
        return convertCompanyToDto(companyRepository.findByIdAndDeletedFalse(id));
    }

    public CompanyDto saveCompany(CompanyDto companyDto) {
        return convertCompanyToDto(companyRepository.save(convertCompanyDtoToEntity(companyDto)));
    }

    public void deleteCompany(Long id) {
        Company company = companyRepository.findByIdAndDeletedFalse(id);
        company.setDeleted(true);
        companyRepository.save(company);
        List<Department> departmentList = departmentRepository.findAllByCompanyIdAndDeletedFalse(id);
        for (Department department : departmentList) {
            department.setDeleted(true);
            departmentRepository.save(department);
            List<Employee> employeeList = employeeRepository.findAllByDepartmentIdAndDeletedFalse(id);
            for (Employee employee : employeeList) {
                employee.setDeleted(true);
                employeeRepository.save(employee);
            }
        }
    }

    private CompanyDto convertCompanyToDto(Company company) {
        List<DepartmentDto> departmentsDto = company.getDepartments().stream().filter(e -> !e.getDeleted()).map(departmentService::convertDepartmentToDto).collect(Collectors.toList());
        return new CompanyDto(company.getId(), company.getAddress(), company.getCity(), company.getCompanyCode(), company.getEmail(), company.getName(), company.getPhone(), company.getPostalCode(), company.getVatCode(), departmentsDto);
    }

    private Company convertCompanyDtoToEntity(CompanyDto companyDto) {
        List<Department> departments = new ArrayList<>();
        if (companyDto.getDepartments() != null) {
            departments = companyDto.getDepartments().stream().map(departmentService::convertDepartmentDtoToEntity).collect(Collectors.toList());
        }
        return new Company(companyDto.getId(), companyDto.getAddress(), companyDto.getCity(), companyDto.getCompanyCode(), companyDto.getEmail(), companyDto.getName(), companyDto.getPhone(), companyDto.getPostalCode(), companyDto.getVatCode(), departments);
    }
}
