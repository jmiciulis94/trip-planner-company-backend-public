package eu.sts.tripplannercompany.backend.services;

import eu.sts.tripplannercompany.backend.constants.Role;
import eu.sts.tripplannercompany.backend.dto.AccountDto;
import eu.sts.tripplannercompany.backend.entities.Account;
import eu.sts.tripplannercompany.backend.repositories.AccountRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<AccountDto> getAccounts() {
        return accountRepository.findAllByDeletedFalse().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    public AccountDto getAccount(Long id) {
        return convertToDto(accountRepository.findByIdAndDeletedFalse(id));
    }

    public AccountDto save(AccountDto accountDto) {
        return convertToDto(accountRepository.save(convertToEntity(accountDto)));
    }

    public void delete(Long id) {
        Account account = accountRepository.findByIdAndDeletedFalse(id);
        account.setDeleted(true);
        accountRepository.save(account);
    }

    private AccountDto convertToDto(Account account) {
        return new AccountDto(account.getId(), account.getFirstName(), account.getLastName(), account.getEmail(), account.getPassword(), formatRoleToString(account.getRole()), account.getPhone(), account.getAvatar());
    }

    private Account convertToEntity(AccountDto accountDto) {
        return new Account(accountDto.getId(), accountDto.getFirstName(), accountDto.getLastName(), accountDto.getEmail(), accountDto.getPassword(), accountDto.getRole(), accountDto.getPhone(), accountDto.getAvatar());
    }

    private String formatRoleToString(Role role) {
        switch (role) {
            case SUPER_ADMIN:
                return "SuperAdmin";
            case USER:
                return "User";
            default:
                return "Admin";
        }
    }
}
