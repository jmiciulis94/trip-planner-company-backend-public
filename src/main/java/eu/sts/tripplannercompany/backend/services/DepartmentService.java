package eu.sts.tripplannercompany.backend.services;

import eu.sts.tripplannercompany.backend.dto.DepartmentDto;
import eu.sts.tripplannercompany.backend.dto.EmployeeDto;
import eu.sts.tripplannercompany.backend.entities.Company;
import eu.sts.tripplannercompany.backend.entities.Department;
import eu.sts.tripplannercompany.backend.entities.Employee;
import eu.sts.tripplannercompany.backend.repositories.CompanyRepository;
import eu.sts.tripplannercompany.backend.repositories.DepartmentRepository;
import eu.sts.tripplannercompany.backend.repositories.EmployeeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    private final CompanyRepository companyRepository;
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;
    private final EmployeeService employeeService;

    public DepartmentService(CompanyRepository companyRepository, DepartmentRepository departmentRepository, EmployeeRepository employeeRepository, EmployeeService employeeService) {
        this.companyRepository = companyRepository;
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
        this.employeeService = employeeService;
    }

    public List<DepartmentDto> getDepartments() {
        return departmentRepository.findAllByDeletedFalse().stream().map(this::convertDepartmentToDto).collect(Collectors.toList());
    }

    public DepartmentDto getDepartment(Long id) {
        return convertDepartmentToDto(departmentRepository.findByIdAndDeletedFalse(id));
    }

    public DepartmentDto saveDepartment(DepartmentDto departmentDto) {
        return convertDepartmentToDto(departmentRepository.save(convertDepartmentDtoToEntity(departmentDto)));
    }

    public void deleteDepartment(Long id) {
        Department department = departmentRepository.findByIdAndDeletedFalse(id);
        department.setDeleted(true);
        departmentRepository.save(department);
        List<Employee> employeeList = employeeRepository.findAllByDepartmentIdAndDeletedFalse(id);
        for (Employee employee : employeeList) {
            employee.setDeleted(true);
            employeeRepository.save(employee);
        }
    }

    public DepartmentDto convertDepartmentToDto(Department department) {
        List<EmployeeDto> employeesDto = department.getEmployees().stream().filter(e -> !e.getDeleted()).map(employeeService::convertEmployeeToDto).collect(Collectors.toList());
        return new DepartmentDto(department.getId(), department.getName(), department.getCompany().getId(), department.getCompany().getName(), employeesDto);
    }

    public Department convertDepartmentDtoToEntity(DepartmentDto departmentDto) {
        Company company = companyRepository.findByIdAndDeletedFalse(departmentDto.getCompanyId());
        List<Employee> employees = new ArrayList<>();
        if (departmentDto.getEmployees() != null) {
            employees = departmentDto.getEmployees().stream().map(employeeService::convertEmployeeDtoToEntity).collect(Collectors.toList());
        }
        return new Department(departmentDto.getId(), departmentDto.getName(), company, employees);
    }
}
