INSERT INTO
    department (name, company_id, deleted)
VALUES
    ('devbridge_kaunas', 2,  FALSE),
    ('devbridge_vilnius', 2, FALSE),
    ('devbridge_klaipeda', 2,  FALSE),
    ('sourcery_academy', 2, FALSE),
    ('kayak_austria', 1, FALSE),
    ('kayak_kaunas', 1, TRUE);