INSERT INTO
    company (address, city, company_code, email, name, phone, postal_code, vat_code, deleted)
VALUES
    ('Pakraščio g. 69', 'Kaunas', 952592, 'smart-tech-solutions@gmail.com', 'smart-tech-solutions', '+37083264417', 46366, 5435, FALSE),
    ('Savanorių pr. 369', 'Vilnius', 643623, 'smart-tech-solutions@gmail.com', 'devbridge', '+37080367749', 48643, 6234, FALSE),
    ('Savanorių pr. 122', 'Kaunas', 745843, 'smart-tech-solutions@gmail.com', 'kayak', '+37064322113', 48655, 7846, FALSE);
