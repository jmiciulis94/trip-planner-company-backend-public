CREATE TABLE company (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    address TEXT NOT NULL,
    city TEXT NOT NULL,
    company_code BIGINT NOT NULL,
    email VARCHAR(255) NOT NULL,
    name TEXT NOT NULL,
    phone VARCHAR(15) NOT NULL,
    postal_code BIGINT,
    vat_code BIGINT NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE department (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    company_id BIGINT NOT NULL REFERENCES company (id),
    name TEXT NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE employee (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    department_id BIGINT NOT NULL REFERENCES department (id),
    card_id BIGINT,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    phone VARCHAR(15),
    deleted BOOLEAN NOT NULL DEFAULT FALSE
);
