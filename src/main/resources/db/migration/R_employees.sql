INSERT INTO
    employee (card_id, first_name, last_name, phone, deleted, department_id)
VALUES
    (306034, 'Jonas', 'Mičiulis', '+37064522737', FALSE, 1),
    (306064, 'Aurelijus', 'Mičiulis', '+37060426467', FALSE, 1),
    (306075, 'Audrius', 'Mičiulis', '+37069625073', FALSE, 2),
    (302342, 'Tomas', 'Ambrevičius', '+37069502947', TRUE, 2),
    (316087, 'Valdas', 'Eilinis', '+37060744926', FALSE, 3),
    (304362, 'Michael', 'Jordan', '+37065745732', FALSE, 3);